## Hubby Launcher ##

Hubby (Wubby Dubby) Launcher is a self-made launcher with no interesting feature, it's just a scrollable list of clickable icon, nothing more nothing less (I probably have more, but not now). It's practically launcher for ubergrandma.

### Why (does it exist) ? ###

The launcher itself is made as a joke, since most launcher exist on an immersive view (with wallpaper filling the screen and tiny icon floating in harmony), then there's this little rebel I made to proof that... well... bad programming with bad idea equals bad result.

### What (makes it different from other launcher) ? ###

* No nonsense, just a list of your apps
* Makes you look kewl
* Probably also has a side effect of making you look weird

### How (can I contribute) ? ###

* Send me a pull request
* Let me review the changes (but mostly I just let it go through)
* Enjoy your life

### Who (made this) ? ###

* Ayj3y from Telegram, he's the actual inventor of the idea so props to him
* Me, but not by all that much
* There's several picked codes from StackOverflow that sits around in my HDD, I don't know the exact author, but if you're one of them, just contact me and don't sue me please